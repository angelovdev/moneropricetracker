module Main exposing (..)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode as Decode exposing (..)
import Round
import Time



-- MAIN


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type Model
    = Failure
    | Loading
    | Success String


init : () -> ( Model, Cmd Msg )
init _ =
    ( Loading, getPrice )



-- UPDATE


type Msg
    = QueryPrice
    | GotPrice (Result Http.Error String)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        QueryPrice ->
            ( Loading, getPrice )

        GotPrice result ->
            case result of
                Ok url ->
                    ( Success url, Cmd.none )

                Err _ ->
                    ( Failure, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Time.every 60000 (always QueryPrice)



-- VIEW


view : Model -> Html Msg
view model =
    div [ class "wrapper" ]
        [ div [ class "price" ]
            [ h1 [] [ text "Monero Price (EUR)" ]
            , viewPrice model
            ]
        ]


viewPrice : Model -> Html Msg
viewPrice model =
    case model of
        Failure ->
            div []
                [ text "I could not load the price for some reason. "
                ]

        Loading ->
            text "Loading..."

        Success price ->
            div []
                [ p [ class "current-price" ] [ text ("€" ++ Round.round 0 (Maybe.withDefault 0 <| String.toFloat price)) ]
                ]



-- HTTP


getPrice : Cmd Msg
getPrice =
    Http.get
        { url = "https://api.kraken.com/0/public/Ticker?pair=XXMRZEUR"
        , expect = Http.expectJson GotPrice priceDecoder
        }


priceDecoder : Decoder String
priceDecoder =
    field "result" (field "XXMRZEUR" (field "c" (Decode.index 0 Decode.string)))
